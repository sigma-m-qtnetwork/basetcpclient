#ifndef MYCLIENT_H
#define MYCLIENT_H

#include <QObject>
#include <QTcpSocket>

class MyClient : public QObject
{
    Q_OBJECT
private:
    QTcpSocket *connection;
    QString nickname;
public:
    explicit MyClient(QString hostAddres,
                      unsigned int hostPort,
                      QObject *parent = nullptr);
    void sendMessage(QString message);
    void setNickname(QString value);

private slots:
    void newData();
};

#endif // MYCLIENT_H
