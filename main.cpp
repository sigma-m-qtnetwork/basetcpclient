#include <QCoreApplication>

#include "myclient.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    MyClient client("localhost", 8085);

    client.setNickname("Alexandr");

    client.sendMessage("Hello world!");

    return a.exec();
}
