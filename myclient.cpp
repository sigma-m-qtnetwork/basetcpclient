#include "myclient.h"

#include <QDataStream>

void MyClient::setNickname(QString value)
{
    nickname = value;
}

MyClient::MyClient(QString hostAddres,
                   unsigned int hostPort,
                   QObject *parent) : QObject(parent)
{
    this->connection = new QTcpSocket(this);
    this->connection->connectToHost(hostAddres, hostPort);

    connect(this->connection, SIGNAL(readyRead()), this, SLOT(newData()));
}

void MyClient::sendMessage(QString message)
{
    QByteArray outputBuffer;
    QDataStream outStream(&outputBuffer, QIODevice::WriteOnly);

    outStream << this->nickname << message;

    this->connection->write(outputBuffer);
}

void MyClient::newData()
{
    QDataStream inputStream;
    QString nickname;
    QString messageText;

    inputStream.setDevice(this->connection);
    inputStream.setVersion(QDataStream::Qt_5_0);

    inputStream >> nickname >> messageText;

    qInfo() << nickname << messageText;
}
